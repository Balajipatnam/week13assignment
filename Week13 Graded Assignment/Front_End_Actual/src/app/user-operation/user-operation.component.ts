import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-operation',
  templateUrl: './user-operation.component.html',
  styleUrls: ['./user-operation.component.css']
})
export class UserOperationComponent implements OnInit {
  
  RegUser:string="";
  LogUser:string="";

  UserLogRef = new FormGroup({
    useremail: new FormControl(),
    password:new FormControl()
  })
  
  UserRegRef = new FormGroup({
    userfullname: new FormControl(),
    useremail:new FormControl(),
     password:new FormControl(),
    contact:new FormControl(),
    gender: new FormControl(),
    address:new FormControl(),
    city: new FormControl(),
    state:new FormControl()
  })
  constructor(public usSer:UserService,public route:Router) { }

  ngOnInit(): void {
  }
  
  userLogin():void{
    this.usSer.userSignIn(this.UserLogRef.value).subscribe(result=>{
      if(result.startsWith("Welcome")){
        sessionStorage.setItem("usname",this.UserLogRef.value.useremail);
        this.route.navigate(["userhome"]);
      }
      else{
        this.LogUser=result;
        this.UserLogRef.reset();} }
      ,err=>console.log(err),()=>console.log("User Login"));
   
  }

  userRegister():void{
   this.usSer.userSignUp(this.UserRegRef.value).subscribe(res=>this.RegUser=res,err=>console.log(err),()=>console.log("Register User"));
   this.UserRegRef.reset();
  }

}
