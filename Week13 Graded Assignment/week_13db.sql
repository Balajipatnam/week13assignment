create database week_13;
use week_13;
create table adminentity(email varchar(60) primary key not null,password varchar(50) not null,username varchar(50));
create table userentity(id bigint primary key not null auto_increment,userfullname varchar(50) not null,useremail varchar(60) unique not null,password varchar(50) not null,contact double,gender varchar(30),address varchar(300),city varchar(30),state varchar(30));
create table product(pid bigint primary key not null,pname varchar(50) not null,pdesc varchar(200),pimage varchar(1500),price double);
