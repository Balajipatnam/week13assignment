package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.UserEntity;

@Repository
public interface UserDao extends JpaRepository<UserEntity, Integer>{
  
	public UserEntity findByEmailAndPassword(@Param("email") String user,@Param("password") String pass);
	public UserEntity findByEmailOrPassword(@Param("email") String user,@Param("password") String pass);
	
}
