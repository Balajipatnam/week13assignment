package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class ProductEntity {

 @Id	
 private int pid;
 private String pname;
 private String pdesc;
 private String pimage;
 private float price;
 
 
public ProductEntity() {
	super();
	// TODO Auto-generated constructor stub
}


public int getPid() {
	return pid;
}


public void setPid(int pid) {
	this.pid = pid;
}


public String getPname() {
	return pname;
}


public void setPname(String pname) {
	this.pname = pname;
}


public String getPdesc() {
	return pdesc;
}


public void setPdesc(String pdesc) {
	this.pdesc = pdesc;
}


public String getPimage() {
	return pimage;
}


public void setPimage(String pimage) {
	this.pimage = pimage;
}


public float getPrice() {
	return price;
}


public void setPrice(float price) {
	this.price = price;
}


@Override
public String toString() {
	return "ProductEntity [pid=" + pid + ", pname=" + pname + ", pdesc=" + pdesc + ", pimage=" + pimage + ", price="
			+ price + "]";
}

	
}
