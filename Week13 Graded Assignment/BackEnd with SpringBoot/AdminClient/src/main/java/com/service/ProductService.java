package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.ProductEntity;
import com.dao.ProductDao;

@Service
public class ProductService {

	@Autowired
	ProductDao productDao;
	
	
     public String createProduct(ProductEntity prod) {
		
		if(productDao.existsById(prod.getPid())) {
			return "This ID already Exists, Please Give different ID";
		}
		else {
			productDao.save(prod);
			return "Product Created successfully";
		}
	}
	
	public List<ProductEntity> retrieveProducts(){
		return productDao.findAll();
	}
	
	public String updateProduct(ProductEntity prod) {
		
		if(!productDao.existsById(prod.getPid())) {
			return "This ID not Exist, Please give correct ID";
		}
		else {
			ProductEntity pd = productDao.getById(prod.getPid());
			pd.setPname(prod.getPname());
			pd.setPdesc(prod.getPdesc());
			pd.setPimage(prod.getPimage());
			pd.setPrice(prod.getPrice());
			productDao.saveAndFlush(pd);
			return "Product Details updated Successfully";
		}
	}
	
	public String deleteProduct(int id) {
		if(!productDao.existsById(id)) {
			return "This ID not exists, Please give Valid ID";
		}
		else {
			productDao.deleteById(id);
			return "Product in the Id number "+id+" has Deleted successfully";
		}
	}
	
	
}
